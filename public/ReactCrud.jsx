

var StudentAll = React.createClass({


  getInitialState: function () {
    return {date: '', particulars: '', voucherid:'', chequeno:'', debit:'', credit:'', closingbalance:'', id:'',Buttontxt:'Save',data1: []};
  },
   handleChange: function(e) {
        this.setState({[e.target.name]: e.target.value});
    },

  componentDidMount() {

    $.ajax({
       url: "api/getdata",
       type: "GET",
       dataType: 'json',
       ContentType: 'application/json',
       success: function(data) {
         this.setState({data1: data});

       }.bind(this),
       error: function(jqXHR) {
         console.log(jqXHR);

       }.bind(this)
    });
  },

DeleteData(id){
  var studentDelete = {
        'id': id
           };
    $.ajax({
      url: "/api/Removedata/",
      dataType: 'json',
      type: 'POST',
      data: studentDelete,
      success: function(data) {
        alert(data.data);
         this.componentDidMount();

      }.bind(this),
      error: function(xhr, status, err) {
         alert(err);


      }.bind(this),
      });
    },



    EditData(item){
   this.setState({voucherid: item.voucherid,desc:item.desc,amount:item.amount,date:item.date,id:item._id,Buttontxt:'Update'});
     },

   handleClick: function() {

   var Url="";
   if(this.state.Buttontxt=="Save"){
      Url="/api/savedata";
       }
      else{
      Url="/api/Updatedata";
      }
      var studentdata = {
        'voucherid': this.state.voucherid,
        'desc':this.state.desc,
        'amount':this.state.amount,
        'date':this.state.date,
        'id':this.state.id,

    }
    $.ajax({
      url: Url,
      dataType: 'json',
      type: 'POST',
      data: studentdata,
      success: function(data) {
          alert(data.data);
          this.setState(this.getInitialState());
          this.componentDidMount();

      }.bind(this),
      error: function(xhr, status, err) {
         alert(err);
      }.bind(this)
    });
  },

  render: function() {
      return (
      <div  className="container"  style={{marginTop:'50px'}}>
       <p className="text-center" style={{fontSize:'25px'}}><b> Petty Sheet Form</b></p>
  <form>
    <div className="col-sm-12 col-md-12"  style={{marginLeft:'400px'}}>
  <table className="table-bordered">
     <tbody>
    <tr>
      <td><b>Voucher Id</b></td>
      <td>
         <input className="form-control" type="text" value={this.state.voucherid}    name="voucherid" onChange={ this.handleChange } />
          <input type="hidden" value={this.state.id}    name="id"  />
      </td>
    </tr>

    <tr>
      <td><b>Description</b></td>
      <td>
      <input type="text" className="form-control" value={this.state.desc}  name="desc" onChange={ this.handleChange } />
      </td>
    </tr>

    <tr>
      <td><b>Amount</b></td>
      <td>
        <input type="text"  className="form-control" value={this.state.amount}  name="amount" onChange={ this.handleChange } />
      </td>
    </tr>


    <tr>
      <td><b>Date</b></td>
      <td>
        <input type="text"  className="form-control" value={this.state.date}  name="date" onChange={ this.handleChange } />
      </td>
    </tr>

    <tr>
      <td></td>
      <td>
        <input className="btn btn-primary" type="button" value={this.state.Buttontxt} onClick={this.handleClick} />
      </td>
    </tr>

 </tbody>
    </table>
</div>


<div className="col-sm-12 col-md-12 "  style={{marginTop:'50px',marginLeft:'300px'}} >

 <table className="table-bordered"><tbody>
   <tr><th><b>S.No</b></th><th><b>Voucher Id</b></th><th><b>Description</b></th><th><b>Amount</b></th><th><b>Date</b></th><th><b>Edit</b></th><th><b>Delete</b></th></tr>
    {this.state.data1.map((item, index) => (
        <tr key={index}>
           <td>{index+1}</td>
          <td>{item.voucherid}</td>
          <td>{item.desc}</td>
          <td>{item.amount}</td>
          <td>{item.date}</td>
           <td>

           <button type="button" className="btn btn-success" onClick={(e) => {this.EditData(item)}}>Edit</button>
          </td>
          <td>
             <button type="button" className="btn btn-info" onClick={(e) => {this.DeleteData(item._id)}}>Delete</button>
          </td>
        </tr>
    ))}
    </tbody>
    </table>
     </div>
</form>
      </div>

    );
  }
});




ReactDOM.render(<StudentAll  />, document.getElementById('root'))
