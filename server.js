var express = require("express");
var path = require("path");
var mongo = require("mongoose");
var bodyParser = require('body-parser');
var morgan = require("morgan");
var db = require("./config.js");
var cors = require('cors')
//var Reactsheet = require("./react.js");


var app = express();
var port = process.env.port || 7000;
var srcpath  =path.join(__dirname,'/public') ;
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(cors())

//var d = new Date();



var mongoose = require('mongoose');
var Schema = mongoose.Schema;

////////////////////////////////////////////////////////////////

var branchSchema = new Schema({
  location: { type: String },
  branchmanager: { type: String },
  wallet: { type: Number },
  date: { type: Date, defaultTo: Date.now  },
  status: { type: String,
            enum: ['ACTIVE', 'INACTIVE', 'COMPLETED'],
            default: 'ACTIVE' }
});

////////////////////////////////////////////////////////////////

// var walletSchema = new Schema({
//     walletamount: { type: Number},
//     branchid: {type: String},
//     //walletid: {type: String},
//     //_id: {type: String}
// });

/////////////////////////////////////////////////////////////////

var transactionSchema = new Schema({
    date: { type: Date, defaultTo: Date.now },
    particulars: { type: String },
    voucherid: { type: Number },
    chequeno: { type: Number },
    debit: { type: Number },
    credit: { type: Number },
    branchid: { type: String },
    closingbalance: { type: Number }
});

////////////////////////////////////////////////////////////////////

var branchmodel = mongoose.model('branchC', branchSchema, 'branchC');
//var walletmodel = mongoose.model('walletC',walletSchema, 'walletC');
var transactionmodel = mongoose.model('transactionC', transactionSchema, 'transactionC');

//////////////////////////////////////////////////////////////////////
//branchSchema

//api for Insert branch in database

app.post("/api/savebranch",function(req,res){

    var branch = new branchmodel();
        branch.location = req.body.location;
        branch.branchmanager = req.body.branchmanager;
        branch.wallet = req.body.wallet;
        branch.date = req.body.date;
        //  status: "ACTIVE";
        //branch.status = req.body.status;
        branch.save(function(err,data){
              if(err){
                  res.send(err);
              }
              else{

                   res.json({data:data});
                   //res.send({data:"Branch has been Inserted..!!"});
              }
          });
  })

//////////////////////////////////////////////////////////////////////////

//api for get branch from database

app.get("/api/getbranch",function(req,res){
 branchmodel.find({},function(err,data){
            if(err){
                res.send(err);
            }
            else{
                res.json({data:data});
                //res.send(data);
                }
        });
})

/////////////////////////////////////////////////////////////////////////

//api for get branch by id from database

app.get('/api/getbranch/:id',function(req,res){
  console.log('getting one branch');
  branchmodel.findOne({
    _id:req.params.id
  })
  .exec(function(err,data){
    if(err){
      res.send('error occured');
    }else{
      console.log(data);
      //res.send(data);
        res.json({data:data});
    }
  })
})

//////////////////////////////////////////////////////////////////////////

//api for Delete branch from database

app.put("/api/removebranch/:id",function(req,res){
 branchmodel.findByIdAndUpdate({ _id: req.params.id }, {status:'INACTIVE'},function(err) {

            if(err){
                res.send(err);
            }
            else{

                   res.json({data:"Branch has been Deleted..!!"});
               }
        });
})

///////////////////////////////////////////////////////////////////////////

//api for Update branch in database

app.put("/api/updatebranch/:id",function(req,res){

 branchmodel.findByIdAndUpdate(req.params.id, { location:req.body.location, branchmanager: req.body.branchmanager, wallet:req.body.wallet, date:req.body.date, status:req.body.status},
function(err,data) {

 if (err) {
 res.send(err);
 return;
 }
 else{
 console.log(data);
 res.json({data:data});
 //res.send(data);
 }
 });
 });

//////////////////////////////////////////////////////////////////////////////

//transactionSchema

//api for insert data in transaction database

app.post("/api/savedata",function(req,res){
    let updatedclosingbalance;
    branchmodel.findOne({
      _id:req.body.branchid
    })
    .exec(function(err,data){
      if(err){
        return res.send('error occured');
      }
      else{
        //res.json(data);
        console.log('wallet is' + data.wallet);
        updatedclosingbalance = parseInt(data.wallet) - parseInt(req.body.debit) + parseInt(req.body.credit);
        console.log('upcb = '+ updatedclosingbalance);

     var transaction = new transactionmodel(
     {
       date : req.body.date,
       particulars : req.body.particulars,
       voucherid : req.body.voucherid,
       chequeno : req.body.chequeno,
       debit : req.body.debit,
       credit : req.body.credit,
       branchid : req.body.branchid,
       closingbalance : updatedclosingbalance
     })


   transaction.save(function(err,data){
                 if(err){
                     res.send(err);
                 }else{
            console.log('transaction',data);
           branchmodel.findByIdAndUpdate(req.body.branchid, {wallet:updatedclosingbalance},
           function(err,data) {
            if (err) {
             return res.send(err);
            }
            else{
              branchmodel.findOne({
              _id:data._id
              })

              .exec(function(err,data){
                console.log('updated wallet is' +  data.wallet);

              })
           }
        })

           res.send({data:data});
           }
         });
}
});
})



///////////////////////////////////////////////////////////////////////////////////////

 //api for get data from database

 app.get("/api/getdata",function(req,res){
  transactionmodel.find({},function(err,data){
             if(err){
                 res.send(err);
             }
             else{
                 res.json({data:data});
                 }
         });
 })

/////////////////////////////////////////////////////////////////////////////////

 //api for get data by id from database

 app.get('/api/getdata/:id',function(req,res){
   console.log('getting one data');
   transactionmodel.findOne({
     _id:req.params.id
   })
   .exec(function(err,data){
     if(err){
       res.send('error occured');
     }else{
       console.log(data);
       res.json({data:data});
       //res.send(data);
     }
   })
 })

/////////////////////////////////////////////////////////////////////////////////

 //api for Delete data from database

 // app.delete("/api/removedata/:id",function(req,res){
 //  transactionmodel.remove({ _id: req.params.id }, function(err) {
 //             if(err){
 //                 res.send(err);
 //             }
 //             else{
 //                    res.json({data:"Record has been Deleted..!!"});
 //                }
 //         });
 // })

//////////////////////////////////////////////////////////////////////////////

 //api for Update data in database
 // app.put("/api/updatedata/:id",function(req,res){
 //   transactionmodel.findOne({
 //     _id:req.params.id
 //   }).exec(function(err,data){
 //     //console.log(err)
 //     //console.log(data)
 //   var lastclosingbalance = transactionmodel.find().sort({_id}.closingbalance);
 //   var updatedclosingbalance = parseInt(closingbalance) - parseInt(req.body.debit) + parseInt(req.body.credit);
 //  transactionmodel.findByIdAndUpdate(req.params.id, { date: req.body.date, particulars: req.body.particulars, voucherid:req.body.voucherid , chequeno:req.body.chequeno, credit:req.body.credit, debit:req.body.debit, closingbalance:updatedclosingbalance},
 // function(err,data) {
 //
 //  if (err) {
 //  res.send(err);
 //  return;
 //  }
 //  console.log(data);
 //  res.json(data);
 //  res.send({data:"data has been Updated"});
 //  });
 //  });
 // })

////////////////////////////////////////////////////////////////////////////////////////////////

//api for get all transactions from branchid

app.get("/api/getbranchdata/:id",function(req,res){
 transactionmodel.find({branchid:req.params.id},function(err,data){
            if(err){
                res.send(err);
            }
            else{
                res.json({data:data});
                //res.send(data);
                }
        });
})







////////////////////////////////////////////////////////////////////////////

 //walletSchema

 //var walletmodel = mongoose.model('walletC',walletSchema, 'wallet');

 //api for Insert wallet in branch database
 // app.post("/api/savewallet",function(req,res){
 //
 //     var wallet = new walletmodel();
 //         wallet.walletamount = req.body.walletamount;
 //         wallet.branchid = req.body.branchid
 //         wallet.save(function(err,data){
 //               if(err){
 //                   res.send(err);
 //               }
 //               else{
 //                    res.send({data:"wallet has been Inserted..!!"});
 //               }
 //           });
 //   })

/////////////////////////////////////////////////////////////////////////////////

   //api for get wallet from branch database
  //  app.get("/api/getwallet",function(req,res){
  //   walletmodel.find({},function(err,data){
  //              if(err){
  //                  res.send(err);
  //              }
  //              else{
  //                  res.send(data);
  //                  }
  //          });
  //  })

/////////////////////////////////////////////////////////////////////////////

   //api for get data by id from database
  //  app.get('/api/getwallet/:id',function(req,res){
  //    console.log('getting one wallet');
  //    walletmodel.findOne({
  //      _id:req.params.id
  //    })
  //    .exec(function(err,data){
  //      if(err){
  //        res.send('error occured');
  //      }else{
  //        console.log(data);
  //        res.send(data);
  //      }
  //    })
  //  })

///////////////////////////////////////////////////////////////////////////

   //api for Delete wallet from branch database
  //  app.delete("/api/removewallet/:id",function(req,res){
  //   walletmodel.findOneAndRemove({ _id: req.params.id }, function(err) {
  //              if(err){
  //                  res.send(err);
  //              }
  //              else{
  //                     res.send({data:"Wallet has been Deleted..!!"});
  //                 }
  //          });
  //  })

///////////////////////////////////////////////////////////////////////////

   //api for Update wallet in branch database
 // app.put("/api/updatewallet/:id",function(req,res){
 //  walletmodel.findOneAndUpdate(req.params.id, {walletamount:req.body.walletamount, branchid:req.body.branchid},
 // function(err, data) {
 //  if (err) {
 //  res.send(err);
 //  return;
 //  }
 // console.log(data)
 //  res.send({data:'Wallet has been Updated'});
 //  //res.send(data);
 //  });
 // })

//////////////////////////////////////////////////////////////////////////////

 //transactionSchema

 //var transactionmodel = mongoose.model('transactionC', transactionSchema, 'transaction');

 //api for Insert data in database


 // app.post("/api/savedata",function(req,res){
 //    transactionmodel.findOne({}, 'closingbalance', function(err,lastclosingbalance){
 //       if(err){
 //           return res.send(err);
 //       }
 //       console.log(lastclosingbalance);
 //       var updatedclosingbalance = parseInt(lastclosingbalance.closingbalance)
 //                        - parseInt(req.body.debit) + parseInt(req.body.credit);
 //       console.log(updatedclosingbalance);
 //       var transaction = new transactionmodel(
 //         {
 //           date : req.body.date,
 //           particulars : req.body.particulars,
 //           voucherid : req.body.voucherid,
 //           chequeno : req.body.chequeno,
 //           debit : req.body.debit,
 //           credit : req.body.credit,
 //           branchid : req.body.branchid,
 //           wallet: branchid.wallet,
 //           closingbalance : updatedclosingbalance
 //         });
 //
 //
 //          transaction.save(function(err,data){
 //               if(err){
 //                   res.send(err);
 //               }
 //               else{
 //                    res.json(data);
 //                    //res.send({data:"Record has been Inserted..!!"});
 //           }
 //         });
 //
 //  }).sort({"_id":-1});
 //
 // })




































// //api for get data from database
// app.get("/api/getdata",function(req,res){
//  trwalletbrmodel.find({},function(err,data){
//             if(err){
//                 res.send(err);
//             }
//             else{
//                 res.send(data);
//                 }
//         });
// })
//
//
// //api for get data by id from database
// app.get('/api/getdata/:id',function(req,res){
//   console.log('getting one data');
//   trwalletbrmodel.findOne({
//     _id:req.params.id
//   })
//   .exec(function(err,data){
//     if(err){
//       res.send('error occured');
//     }else{
//       console.log(data);
//       res.send(data);
//     }
//   })
// })
//
//
// //api for Delete data from database
// app.post("/api/Removedata",function(req,res){
//  trwalletbrmodel.remove({ _id: req.body.id }, function(err) {
//             if(err){
//                 res.send(err);
//             }
//             else{
//                    res.send({data:"Record has been Deleted..!!"});
//                }
//         });
// })
//
//
// //api for Update data from database
// //var datetime = new Date(day,month,year,hour,minute,second);
//
// app.post("/api/Updatedata",function(req,res){
//   trwalletbrmodel.findOne({
//     _id:req.body.id
//   }).exec(function(err,data){
//     //console.log(err)
//     console.log(data)
//     var wa = data.walletamount
//     var d = new Date();
//
//   var uv = parseInt(wa) - parseInt(req.body.debit) + parseInt(req.body.credit);
//  trwalletbrmodel.findByIdAndUpdate(req.body.id, { sno:  req.body.sno, date: data.d, particulars: req.body.particulars, voucherid:req.body.voucherid , chequeno:req.body.chequeno, credit:req.body.credit, debit:req.body.debit, cb:wa},
// function(err,data) {
//
//  if (err) {
//  res.send(err);
//  return;
//  }
//  console.log(data)
//  res.send(data);
//  });
//  });
// })
//
//
// //api for Insert data from database
// app.post("/api/savedata",function(req,res){
//   trwalletbrmodel.findOne({
//     _id:req.body.id
//   }).exec(function(err,data){
//     //console.log(err)
//     //get the cash balance of the latest entry in this particular branch.
//     console.log(data)
//     //var wa = data.walletamount
//   //   var wa = db.trwalletbrmodel.find().sort({_id:1});
//   //  var uv = parseInt(wa) - parseInt(req.body.debit) + parseInt(req.body.credit);
//   //   var m = new trwalletbrmodel();
//
//
//
//
//        m.date=req.body.date;
//        m.particulars=req.body.particulars;
//        m.voucherid=req.body.voucherid;
//        m.chequeno=req.body.chequeno;
//        m.credit=req.body.credit;
//        m.debit=req.body.debit;
//        m.cb=uv;
//         m.save(function(err,data){
//             if(err){
//                 res.send(err);
//             }
//             else{
//                  //res.send({data:"Record has been Inserted..!!"});
//                 console.log(data);
//                  res.send(data);
//             }
//         });
// })

// call by default index.html page
app.get("*",function(req,res){
    res.sendFile(srcpath +'/index.html');
})

//server stat on given port
app.listen(port,function(){
    console.log("server started on port"+ port);
})
