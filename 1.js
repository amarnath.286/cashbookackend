




const vogels = require('vogels');
const Joi = require('joi');
const logger = require('../../config/logger');
const kafka = require("kafka-node"),
  Producer = kafka.Producer,
  client = new kafka.Client(process.env.KAFKA_CLIENT),
  producer = new Producer(client);
const Lr = vogels.define("load_receipts", {
  hashKey: "lrNumber",
  timestamps: true,
  schema: {
    id: vogels.types.uuid(), // dynamo db uuid
    lrNumber: Joi.string().required(),
    consignor: Joi.object().keys({
      name: Joi.string(),
      location: Joi.string(),
    }),
    consignee: Joi.object().keys({
      name: Joi.string(),
      location: Joi.string(),
    }),
    truckNumber: Joi.string(),
    truckType: Joi.string(),
    // lrDate: Joi.date(),
    gatePass: Joi.string(),
    from: Joi.object(),
    to: Joi.object(),
    sendSms: Joi.boolean(),
    owner: Joi.object().keys({
      name: Joi.string().allow(null),
      panNumber: Joi.string().allow(null),
      mobile: Joi.number(),
    }),
    goodsInfo: Joi.string(),
    material: Joi.string(),
    driver: Joi.object().keys({
      name: Joi.string().allow(null),
      mobile: Joi.number(),
      licence: Joi.string().allow(null),
      hasSmartPhone: Joi.string().allow(null),
    }),
    consignmentValue: Joi.string(),
    transporter: Joi.object().keys({
      name: Joi.string().allow(null),
      type: Joi.string().allow(null),
      isPrePaidTransporter: Joi.string().allow(null),
    }),
    sealNumbers: Joi.string(),
    dcNumber: Joi.string(),
    invoice: Joi.object(), // data:[array of invoces]
    grossWeight: Joi.number(),
    tareWeight: Joi.number(),
    netWeight: Joi.number(),
    netWtUnits: Joi.string(),
    freightCharges: Joi.number(),//per MT
    totalFreightCharges: Joi.number(),
    wayBillType: Joi.string(),
    wayBillNumber: Joi.string(),
    tds: Joi.object().keys({
      applicable: Joi.string(),
      percentageValue: Joi.number(),
      value: Joi.number(),
    }),

    bankDetailsAdded: Joi.boolean().default(false),
    podReceived: Joi.boolean().default(false),
    invoiceRaised: Joi.boolean().default(false),
    paymentReceived: Joi.boolean().default(false),
    paymentApproved: Joi.boolean().default(false),

    ownerSameAsVendor: Joi.boolean().default(false),
    advanceData: Joi.object().keys({
      freightPercentage: Joi.number(),
      advanceToBePaid: Joi.number(),
      panNumber: Joi.string(),
      cash: Joi.number().allow(null),
      fuelAmount: Joi.number().allow(null),
      fuelType: Joi.string().allow(null),
      fuelReferenceNumber: Joi.string().allow(null),
      fastTagAmount: Joi.number().allow(null),
      fastTagNumber: Joi.string().allow(null),
      chequeAmount: Joi.number().allow(null),
      chequeNumber: Joi.string().allow(null),
      accountHolderName: Joi.string().allow(null),
      bankAccountNumber: Joi.string().allow(null),
      bankBranchName: Joi.string().allow(null),
      bankBranchIFSC: Joi.string().allow(null),
      bankAccountHolderMobile: Joi.string().allow(null),
      accountPaidAmount: Joi.number().allow(null),
      total: Joi.number().allow(null), // sum of above all
      balanceToBePaid: Joi.number(), // balanceToBePaid to advance
      transactionId: Joi.string(),
      transactionDate: Joi.date(),
      notes: Joi.string().allow(null),
    }),
    unloadedDate: Joi.date().allow(null),
    unloadedWeight: Joi.number(),
    tolerance: Joi.number(),
    margin: Joi.number(),
    shortageRate: Joi.number(),
    shortageAmount: Joi.number(),
    fine: Joi.number(),
    customToleranceDesc: Joi.string(),
    balanceData: Joi.object().keys({
      panNumber: Joi.string().allow(null),
      cash: Joi.number().allow(null),
      fuelAmount: Joi.number().allow(null),
      fuelType: Joi.string().allow(null),
      fuelReferenceNumber: Joi.string().allow(null),
      fastTagAmount: Joi.number().allow(null),
      fastTagNumber: Joi.string().allow(null),
      chequeAmount: Joi.number().allow(null),
      chequeNumber: Joi.string().allow(null),
      accountHolderName: Joi.string().allow(null),
      bankAccountNumber: Joi.string().allow(null),
      bankBranchName: Joi.string().allow(null),
      bankAccountHolderMobile: Joi.string().allow(null),
      accountPaidAmount: Joi.number().allow(null),
      bankBranchIFSC: Joi.string().allow(null),
      total: Joi.number().allow(null), // actual balance paid : sum of above all
      transactionId: Joi.string(),
      balanceToBePaid: Joi.number(), // balanceToBePaid to in balance
      transactionDate: Joi.date(),
      notes: Joi.string().allow(null),
    }),
    totalAmount: Joi.number(), // totalAmount before starts trip
    totalAmountAtCloseLr: Joi.number(), // totalAmount At time of CloseLr
    totalAmountAtCreateLr: Joi.number(), // totalAmount At time of CloseLr
    settlement: Joi.boolean().default(false), // when lr close, settlement is true
    settlementDifference: Joi.number(), // settlement difference if any [allow +/_ 20]
    settlementAmount: Joi.number(), // settlement difference if any [allow +/_ 20]
    status: Joi.string()
      .required()
      .default("NEW")
      .valid(
        "NEW",
        "PENDING",
        "POD-RECEIVED",
        "INVOICE-RAISED",
        "PAYMENT-RECEIVED",
        "INACTIVE",
        "SHORTAGE"
      ),
    branch: Joi.string(), // lr branch
    validationStatus: Joi.string().default("RED"),
    validationDesc: Joi.string().allow(null),
    lrType: Joi.string()
      .required()
      .default("QUANTITY")
      .valid("QUANTITY", "TRUCK", "BOTH", "NONE"),
    company: Joi.object(),
    companyId: Joi.string(),
    lane: Joi.object(),
    laneId: Joi.string(),
    indent: Joi.object(),
    indentId: Joi.string(),
    deliveryOrder: Joi.object(),
    deliveryOrderId: Joi.string(),
    deliveryOrderNumber: Joi.string(),
    // truck : Joi.string(),
    // ownerData : Joi.string(),
    clientName: Joi.string(),
    passingWeight: Joi.number(), // not using this nothing but net weight
    materialInfo: Joi.string(),
    GSTIN: Joi.string(),
    panNumber: Joi.string(),
    sellingPrice: Joi.number(),
    advancePaymentDone: Joi.boolean().default(false),
    balancePaymentDone: Joi.boolean().default(false),
    loading: Joi.object(),
    unloading: Joi.object(),
    paymentApproved: Joi.boolean().default(false),
    bankDetails: Joi.object().keys({
      accountHolderName: Joi.string(),
      branchName: Joi.string(),
      accountNumber: Joi.string(),
      accountIFSC: Joi.string(),
      panNumber: Joi.string(),
    }),
    images: Joi.object(),
    orderId: Joi.string(),
    notes: Joi.string().allow(null),
    generatedDate: Joi.date(),
    createdDate: Joi.date(),
    unloadingDate: Joi.date(),
    podReceivedDate: Joi.date(),
    invoiceRaisedDate: Joi.date(),
    paymentReceivedDate: Joi.date(),
    advancePaymentDate: Joi.date(),
    balancePaymentDate: Joi.date(),
    closedDate: Joi.date(),
    updatedDate: Joi.date(),
    createdBy: Joi.object(),
    updatedBy: Joi.object(),
    vendor: Joi.object(),
    remarks: Joi.object(),
    detention: Joi.object(),
    backupData: Joi.object(),
    emergencyData: Joi.object(),
    modelVersion: "1.0",
    exception : Joi.boolean().default(false)
  },
  indexes: [
    {
      hashKey: "status",
      name: "LoadReceiptsStatusIndex",
      type: "global",
    },

		{
			hashKey: 'companyId',
			name: 'LoadReceiptsCompanyIndex',
			type: 'global'
		}
	],

	tableName: 'load_receipts'
});
Lr.after('create', function(data) {
	let ss_payloads = [
		{
			topic: 'prod_lr_singlestream',
			messages: JSON.stringify(data.attrs),
			partition: 0
		}
	];
	// producer.on('ready', function () {
	producer.send(ss_payloads, function(err, data) {
		logger.info('Error', err);
		logger.info('ss create lr payloads written to the kafka');
	});
	// });

	producer.on('error', function(err) {
		logger.info(err);
	});

	logger.info('Lr model post hook!');
});
Lr.after('update', function(data) {
	let ss_payloads = [
		{
			topic: 'prod_lr_singlestream',
			messages: JSON.stringify(data.attrs),
			partition: 0
		}
	];
	// producer.on('ready', function () {
	producer.send(ss_payloads, function(err, data) {
		logger.info('Error', err);
		logger.info('ss create lr payloads written to the kafka');
	});
	// });

	producer.on('error', function(err) {
		logger.info(err);
	});

	logger.info('Lr model post hook!');
});
module.exports = Lr;
